import React from 'react';
import './App.css';
import {
  Navbar, 
  Nav,
  Container,
  Image,
  Row, 
  Col,
  div, 
  Form, 
  Button,
  Card
} from 'react-bootstrap';

///NAVBAR
function Header(){
  return(
        <Navbar bg="white" expand="lg" sticky="top" style={{ height:"70px" }}>
          <Container>
          <Navbar.Brand href="http://localhost:3001/">
            <img
              src="https://bibit.id/img/logoBibitFix.svg"
              width="100"
              height="80"
              className="d-inline-block align-top"/>
          </Navbar.Brand>

          <Nav>
            <Nav.Link href="#Reksadana" style={{ color: 'green', marginLeft: "25px" }}>Reksadana</Nav.Link>
            <Nav.Link href="#Blog" style={{ color: 'green', marginLeft: "25px" }}>Blog</Nav.Link>
            <Nav.Link href="#FAQ" style={{ color: 'green', marginLeft: "25px" }}>FAQ</Nav.Link>
            <Nav.Link href="#Karir" style={{ color: 'green', marginLeft: "25px" }}>Karir</Nav.Link>
            <Button variant="outline-success" style={{ marginLeft: "25px" }}>Login</Button>
          </Nav>

          </Container>
      </Navbar>
  );
}

///SLIDE AWAL
function Welcome(){
  return(
    <Row>
      <Col style= {{  padding: "110px", paddingTop: "10x  0px" }}>
      <h1 style={{ color: 'green', fontWeight:'bold'}}>INVESTASI REKSA DANA <br/>SECARA OTOMATIS</h1>

      <br/>

      <h4 style={{ color: 'gray', fontfamily: 'serif' }}>Bibit menempatkan uang kamu ke portfolio <br/>reksa dana secara cerdas dan tanpa ribet.</h4>

      <br/>

      <Navbar.Brand href="https://play.google.com/store/apps/details?id=com.bibit.bibitid">
        <img
            src="https://bibit.id/img/playStore.png"
            width="160"
            height="45"
            className="d-inline-block align-top"/>
      </Navbar.Brand>

      <Navbar.Brand href="https://apps.apple.com/id/app/bibit-investasi-reksadana/id1445856964">
        <img
          src="https://bibit.id/img/appStore.png"
          width="160"
          height="45"
          className="d-inline-block align-top"/> 
      </Navbar.Brand>

      <br/><br/>

      <h6 style={{ color: 'gray'}}>Terdaftar dan Diawasi oleh</h6>

      <br/>

      <Navbar.Brand href="https://reksadana.ojk.go.id/Public/APERDPublic.aspx?id=BTB69">
        <img
          src="https://bibit.id/img/ojk.png"
          width="120"
          height="50"
          className="d-inline-block align-top"/>
      </Navbar.Brand>
      </Col>

        <Col>
          <Image src="https://bibit.id/img/mockup.png" style={{ marginLeft:"73px", width:"270px", paddingBottom: "100px", paddingTop:"20px" }} bsPrefix/> 
        </Col>
    </Row>
  );
}

///
function Teknologi(){
  const Slide = (props) => {
    return(
      <div style={{ width: "50 0px", paddingTop: '150px' }}>
        <Image src={props.Image} fluid  width="40"/>
        <h8 style={{ color: '#4E83B8', fontSize: "18px" }}> {props.Title} </h8><br/>
        <br/>
        <p> {props.Subtitle} </p>
      </div>
    );
  }

  return(
    <Row>
      <Row>
        <Col>
          <Image src="https://bibit.id/img/illustration2.svg" fluid width="500"/>
        </Col>

        <Col>
          <Slide 
            Image="https://bibit.id/img/processorIcon.png"
            Title="TEKNOLOGI YANG DIDUKUNG OLEH RISET PEMENANG NOBEL PRIZE"
            Subtitle="Teknologi kami menggunakan pendekatan Teori Modern Portfolio yang diperkenalkan oleh ekonom Harry Markowitz yang sudah terbukti dapat menjaga resiko dan memaksimalkan keuntungan kamu lewat diversifikasi."
          />
        </Col>
      </Row>

      <Row style={{ marginRight: "70px", marginLeft: "30px" }}>
        <Col style={{ textAlign: "Right" }}>
          <Slide 
            Image="https://bibit.id/img/formulaIcon.png"
            Title="INVESTASI YANG DIRANCANG KHUSUS UNTUK KAMU"
            Subtitle="Setiap orang mempunyai profil yang berbeda. Kita bantu menempatkan uang kamu ke portfolio reksadana yang khusus dibuat untuk kamu sesuai dengan umur, penghasilan dan toleransi kamu terhadap resiko. Secara otomatis"
          />
        </Col>

        <Col>
          <Image src="https://bibit.id/img/illustration3.svg" fluid width="500"/>
        </Col>
      </Row>

      <Row>
       <Col>
          <Image src="https://bibit.id/img/illustration4.svg" fluid width="530"/>
        </Col>

        <Col>
          <Slide 
            Image="https://bibit.id/img/machineIcon.png"
            Title="REBALANCING OTOMATIS"
            Subtitle="Teknologi kami akan bekerja dan memantau portfolio kamu secara otomatis untuk mempertahankan alokasi optimal kamu."
          />
        </Col>
      </Row>

      <Row style={{ marginLeft: "100px" }}>
        <Col style={{ textAlign: "Right", paddingBottom: "200px" }}>
          <Slide 
            Image="https://bibit.id/img/fastIcon.png"
            Title="BUKA AKUN SECARA INSTAN"
            Subtitle="Pembukaan rekening secara digital dan dapat diselesaikan dalam hitungan menit. Tanpa formulir, tanpa financial planner, tanpa spreadsheet. Tanpa ribet."
          />
        </Col>

        <Col style={{ width: "60px", paddingTop: "70px" }}>
          <Image src="https://bibit.id/img/illustration5.svg" fluid width="300"/>
        </Col>
      </Row>
    </Row>
  );

}

/////
function Langkah(){
  const  Rutif = (props) => {
    return(
      <div class="card" style={{ width: "300px", height: "220px", 
      borderRadius: "20px", boxShadow: "0px 1px #F0F0F0" , marginLeft: "50px" }}>
        <div class="card-body">
          <h6 style={{ textAlign: "center" }}>{props.Title}</h6>
          <p>{props.Subtitle}</p>
        </div>
      </div>
    );
  }

  return(
    <Container>
    <Row>
      <Col>
        <Rutif
        Title="Kita Pelajari Profil Kamu"
        Subtitle="Kita ingin mengetahui tujuan investasi kamu, umur kamu dan pandangan kamu terhadap resiko"/>
      </Col>

      <Col>
        <Rutif
        Title="Kita Bangun Portfolio Investasi Kamu"
        Subtitle="Kita berikan saran investasi yang optimal berdasarkan toleransi kamu terhadap resiko"/>
      </Col>

      <Col>
        <Rutif
        Title="Uang Kamu Mulai Bekerja Lebih Keras Untuk Kamu"
        Subtitle="Set up sekali dan mulai lihat bibit masa depan kamu bertumbuh tanpa ribet"/>
      </Col>
    </Row>
    </Container>
  );
}

function Keuntungan(){
  return(
    <div style={{ textAlign: "center", padding: "100px" }}>
      <Image src= "https://bibit.id/img/ideaIcon.png" fluid width= "45px"/>
      <br/><br/>
      <h6 style={{ color: '#4E83B8' }}> KEUNTUNGAN INVESTASI DENGAN BIBIT </h6>
    </div>

  );
}


function Feature(){
  const  Sorot = (props) => {
    return(
      <div style={{ marginLeft: "70px", marginRight: "40px" }}>
<div class="card" style={{ width: "500px", height: "140px", 
      borderRadius: "15px", boxShadow: "0px 1px #F0F0F0", marginLeft: "10px", marginTop: "20px"}}>
  <div class="row no-gutters">
      <div class="col-md-2">
        <Card.Img variant="top" src={props.Image} style={{ maxWidth: "60px", margin: "20px" }} />
    </div>
      <div class="col-md-8">
      <Card.Body style={{  }}>
        <Card.Title><h6>{props.Title}</h6></Card.Title>
        <Card.Text>
          <p style={{ fontSize: "13px" }}>{props.Subtitle}</p>
        </Card.Text>
      </Card.Body>
    </div>
  </div></div>
  </div>
      
    );
  }

  return(
    <Row>
    <Row>
      <Col>
        <Sorot
          Image="https://bibit.id/img/rpNol.svg"
          Title="Gratis Biaya Komisi"
          Subtitle="Biaya Rp0. Semua pembelian reksadana di Bibit gratis biaya komisi."
        />
      </Col>
    </Row>

    <Row>
      <Col>
        <Sorot
          Image="https://bibit.id/img/minimInvestasi.svg"
          Title="Dana Investasi yang Minim"
          Subtitle="Kamu bisa mulai dengan dana serendah Rp10.000 dulu sampai kamu nyaman berinvestasi. Yang penting mulai."
        />
      </Col>
    </Row>

    <Row>
      <Col>
        <Sorot
          Image="https://bibit.id/img/cairkanInvestasi.svg"
          Title="Cairkan Investasimu Kapan Saja"
          Subtitle="Dana investasi dapat dicairkan kapanpun dengan cepat tanpa penalti."
        />
      </Col>
    </Row>

    <Row>
      <Col>
        <Sorot 
          Image="https://bibit.id/img/taxFree.svg"
          Title="Tidak Dipajak"
          Subtitle=" Berbeda dengan investasi properti ataupun emas. Berbeda dengan investasi properti"
          />
      </Col>
    </Row>
    </Row>
  ); 
}

function Keamanan(){
  const Liad = (props) => {
    return(
      <div style={{ width: "50 0px", paddingTop: '300px' }}>
        <Image src={props.Image} fluid  width="45"/>
        <h7 style={{ color: '#4E83B8' }}> {props.Title} </h7><br/>
        <br/>
        <h7> {props.Subtitle} </h7>
      </div>
    );
  }


  return(
    <Row>
        <Col>
          <Image src="https://bibit.id/img/illustration8.svg" fluid width="500" style={{ paddingTop:"150px" }}/>
        </Col>

        <Col>
          <Liad
            Image="https://bibit.id/img/shieldIcon.png"
            Title="KEAMANAN DANA DARI BANK"
            Subtitle="Bibit tidak menyimpan dana kamu. Dana yang kamu investasikan ditransfer langsung ke kustodian bank dan dikelola oleh perusahaan berlisensi OJK. Dana hanya dapat dicairkan ke rekening atas nama kamu."
          />
        </Col>
    </Row>
  );
}

function Sponsor(){
  return(
  <div style={{ textAlign: "center", paddingTop: "100px" }}>
    <div>
      <h7>Telah diliputi oleh</h7><br/><br/>
        <Image src= "https://bibit.id/img/cnbc.png" style={{ width: "100px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/dailysocial.png" style={{ width: "100px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/deal-street.png" style={{ width: "100px", marginLeft: "50px" }}/>
    </div>
  </div>
  );
}

function Kartu(){
  return(
  <div style={{ paddingTop: "100px", marginLeft: "300px" }}>
  <Card body style={{ width: "700px", borderRadius: "15px", boxShadow: "0px 1px #F0F0F0" , textAlign: "center" }}>
    <Card.Title style={{ color: "green" }}>Mulai Tanam Bibit Investasimu</Card.Title><br/>
    <Navbar.Brand href="https://play.google.com/store/apps/details?id=com.bibit.bibitid">
        <img
            src="https://bibit.id/img/playStore.png"
            width="160"
            height="45"
            className="d-inline-block align-top"/>
      </Navbar.Brand>

      <Navbar.Brand href="https://apps.apple.com/id/app/bibit-investasi-reksadana/id1445856964">
        <img
          src="https://bibit.id/img/appStore.png"
          width="160"
          height="45"
          className="d-inline-block align-top"/> 
      </Navbar.Brand>
  </Card>
  </div>
  );
}

function Sponsorlagi(){
  return(
    <div style={{ textAlign: "center", paddingTop: "100px", paddingBottom: "100px" }}>
      <h7>Partner Kami</h7><br/><br/>
        <Image src= "https://bibit.id/img/manager-investasi/sinarmas.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/schroders.png" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/bni-am.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/victoria.jpg" style={{ width: "130px", marginLeft: "50px" }}/>
        <Image src= "https://bibit.id/img/manager-investasi/mandiri-investasi.png" style={{ width: "100px", marginLeft: "50px" }}/>
  </div>
  );
}

function Footer1(){
  return(
      <Container className="bg">
        <Row>
           <Col>
             <p style={{fontSize:"20px"}}>Masih Ragu dan Memiliki Pertanyaan?</p>
             <h3 style={{fontSize:"20px"}}>Tenang, Kami selalu ada disini khusus buat kamu</h3>

             <p class="text-muted" style={{fontSize:"16px"}}>Hubungi kami kapanpun:</p>

             <ul style={{listStyleType:"none"}}>
               <li><a href="#" style={{color:"#00ab6b",fontSize:"18px"}}>hello@bibit.id</a></li>
               <li>
                   <a href="#" style={{color:"#00ab6b",fontSize:"18px"}}>6221 50864230</a>
                   <h6 style={{fontSize:"14px",color:"#00ab6b"}}>Via WhatsApp</h6>
               </li>
               <li>
                   <p href="#" style={{color:"#00ab6b",fontSize:"18px"}}>Menara Standard Chartered 35th floor </p>
                   <p href="#" style={{color:"#00ab6b",fontSize:"13px",letterSpacing:"1.2px"}}>Jl. Prof. Dr. Satrio No 164 </p>
                   <p style={{color:"#00ab6b",fontSize:"13px",letterSpacing:"1.2px"}}>Jakarta 12930 </p>
               </li>
             </ul>
           </Col> 
           <Col>
             <ul style={{listStyleType:"none"}}>
              <li>
                <h3 style={{color:"#00ab6b",fontSize:"17px"}}>Apa itu redaksana?</h3>
                <p style={{color:"gray"}}>Reksadana merupakan tempat pengelolaan dana atau modal bagi sekumpulan investor untuk berinvestasi dalam instrumen investasi yang tersedia di pasar.</p>
              </li>
              <li><h4 style={{fontSize:"17px",color:"gray"}}>Bagaimana cara kerja reksadana?</h4></li>
              <li><h4 style={{fontSize:"17px",color:"gray"}}>Apa saja resiko yang harus dihadapi saat berinvestasi di reksadana?</h4></li>
              <li><a href="#" style={{color:"#00ab6b"}}>Cari tahu artikel lain -></a></li>
             </ul>
           </Col>
        </Row> 
        <br/>
        <Row>
          <div style={{width:"500px"}}>
          <Col>
            <ul style={{listStyleType:"none"}}>
              <li><p style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>TERDAFTAR DAN DIAWASI OLEH</p></li>
              <li></li>
              <li><a href="#" style={{color:"#00ab6b",fontWeight:"bold"}}>KEP-14/PM.21/2017</a></li>
            </ul>
          </Col>
          </div>
          <Col>
            <ul style={{listStyleType:"none"}}>
              <li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>IKUTI KAMI</h3></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Facebook</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Twitter</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Instagram</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Youtube</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Linkedln</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Telegram</a></li>
            </ul>
          </Col>
          <Col>
            <ul style={{listStyleType:"none"}}>
              <li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>PERUSAHAAN</h3></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Blog</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Karir</a></li>
            </ul>
          </Col>
          <Col>
            <ul style={{listStyleType:"none"}}>
              <li><h3 style={{fontWeight:"bold",fontSize:"15px",color:"#8998aa"}}>DUKUNGAN</h3></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>FAQ</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Kebijakan dan Privasi</a></li>
              <li><a href="#" style={{color:"#8998aa",fontSize:"15px"}}>Syarat dan Ketentuan</a></li>
            </ul>
          </Col>
        </Row>       

      </Container>
    );
}

function Footer(){
  return(
  <Container>
      <div>
         <Row style={{ paddingBottom: "30px",marginTop:"70px"}}>
             <Col><Image src="https://bibit.id/img/logoBibitFix.svg" width="70px" bsPrefix/> Copyright © 2020</Col>
                
             <Col><Image src="https://stockbit.com/assets/img/stockbit.svg" width="70px" bsPrefix style={{cssFloat:"right"}}/></Col>
         </Row>
        
         <Row>
              <p style={{fontSize:"14px",color:"#6b7c93"}}>PT Bibit Tumbuh Bersama (“Bibit”) berlaku sebagai Agen Penjual Efek Reksa Dana (APERD) yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan.</p>

              <p style={{fontSize:"14px",color:"#6b7c93"}}>Semua Investasi mengandung risiko dan kemungkinan kerugian nilai investasi. Kinerja pada masa lalu tidak mencerminkan 
              kinerja di masa depan. Kinerja historikal, expected return dan proyeksi probabilitas disediakan untuk tujuan informasi dan 
              illustrasi. Reksa dana merupakan produk pasar modal dan bukan produk APERD. APERD tidak bertanggung jawab atas risiko 
              pengelolaan portofolio yang dilakukan oleh Manajer Investasi.</p>
         </Row>
      </div>
  </Container>
 );
}

function App(){
  return(
    <div>
      <Header/>
      <Welcome/>
      <Teknologi/>
      <Langkah/>
      <Keuntungan/>
      <Feature/>
      <Keamanan/>
      <Sponsor/>
      <Kartu/>
      <Sponsorlagi/>
      <Footer1/>
      <Footer/>
    </div>
  );
}

export default App;